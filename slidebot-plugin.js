var Slidebot_Plugin = (function () {

  // Main purpose of this script
  function slideChanged(event) {
    console.log('SLIDE CHANGED!');
    let thisEvent = event;
    const callToSlidebotApi = new Promise(function (resolve, reject) {
      //make the call
      jQuery.ajax({
        type: "get",
        url: 'https://slidebot.lndo.site/api/slides/' + thisEvent.indexh,
      });

      console.log(thisEvent.indexh);
    });

    callToSlidebotApi.then(function (value) {
      console.log("Slide #" + value);
      // expected output: "foo"
    });
  }

  return {
    init: function () {
      Reveal.addEventListener('slidechanged', slideChanged);
      console.log('calevans:init');
    }
  };
})();

Reveal.registerPlugin('slidebot_plugin', Slidebot_Plugin);

