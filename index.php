<?php
/*
 * Plugin Name: SlideBot
 * Plugin URI:  https://gitlab.com/calevans/slidebot-plugin
 * Description: It's a plugin...for your slidebot
 * Version:     0.0.3
 * Author:      Cal Evans
 * Author URI:  https://calevans.com
 * Text Domain: slidebot
 * License:     MIT
 */
add_action( 'wp_enqueue_scripts', function() {
	wp_enqueue_script(
		'calevans',
		plugins_url( 'slidebot-plugin.js', __FILE__ ),
		array( 'slide-reveal', 'jquery' ),
		filemtime( dirname( __FILE__ ) . '/slidebot-plugin.js' ),
		true
	);
}
);